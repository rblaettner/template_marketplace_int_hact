#RequireAdmin
#include "XMLDomWrapper.au3"
#include <WinAPIReg.au3>
;
#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_Outfile=32.exe
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****
;
;

	AutoItSetOption( "WinTitleMatchMode", 2 )

	Dim $msgBoxOK = 0
	Dim $msgBoxRetryCancel = 5

	Dim $progressCount = ( -1 )
	Dim Const $progressTotal = 259

;	Default file storage locations ("Resources\...")
;
	Global Const $IC_HOSTNAME = @ComputerName
	Dim Const $APP_SUBFOLDER = "Appointments"
;
	Dim Const $IC_SERVER_DIR = RegRead( "\\" & $IC_HOSTNAME & "\" & "HKLM64\SOFTWARE\Wow6432Node\Interactive Intelligence", "Target" )
	Dim Const $IC_I3_IC_PATH = RegRead( "\\" & $IC_HOSTNAME & "\" & "HKLM64\SOFTWARE\Wow6432Node\Interactive Intelligence", "Value" )

	Dim Const $IC_SHARE_RESOURCES = $IC_SERVER_DIR & "Resources" & "\" & $APP_SUBFOLDER
	Dim Const $IC_LOCAL_RESOURCES = $IC_I3_IC_PATH & "Resources" & "\" & $APP_SUBFOLDER
;

;	==========
;
;	Registry values for future lookup and reference...
;
	Dim Const $DS_ROOT_KEY = "HKLM\SOFTWARE\Interactive Intelligence\EIC\Directory Services\Root"
;
	Dim Const $DS_SERVER_VAL = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "SERVER" )
	Dim Const $DS_CONFIG_VAL = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "CONFIG" )
	Dim Const $DS_SITE_VALUE = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_ROOT_KEY ), "SITE" )
	Dim Const $DS_CONFIG_KEY = ( $DS_ROOT_KEY & $DS_CONFIG_VAL & "\" & "Configuration" )

	Local Const $DS_DATA_SOURCE = "ININ_APPT_CONF"

	$dbOdbcDsn = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_CONFIG_KEY & "\" & "Data Sources" & "\" & $DS_DATA_SOURCE ), "ODBC DSN" )
	$dbOdbcDbo = RegRead( ( "\\" & $IC_HOSTNAME & "\" & $DS_CONFIG_KEY & "\" & "Data Sources" & "\" & $DS_DATA_SOURCE ), "QUAL" )
;
;	==========
;
;	Derivation of Server, Site...
;
	Dim Const $DS_SERVER_NAME = StringReplace( StringReplace( $DS_SERVER_VAL, $DS_CONFIG_VAL, "" ), "\", "" )
	Dim Const $DS_SITE_NAME = StringReplace( $DS_SITE_VALUE, "\", "" )
;
;	=========

;	=========

	; CurrentLicenseAdmin

	_XMLFileOpen( ( $IC_I3_IC_PATH & "CurrentLicenseAdmin.I3LIC" ), "xmlns:ms='urn:schemas-microsoft-com:xslt'" )

	Local $licenseFeatures = _XMLGetField ( "LicenseFiles/LicenseFile/Features/Feature[@name]/@name" )

	If _
	( ( _ArraySearch( $licenseFeatures, "I3_FEATURE_IPA" ) == ( -1 ) ) Or _ ; NOTE: OR vs. AND (inclusive)
	( _ArraySearch( $licenseFeatures, "I3_FEATURE_TEMPLATE_MARKETPLACE_INT_HACT" ) == ( -1 ) ) ) Then

	   Local $retVal = MsgBox( ( $msgBoxOK + 16 + 4096 ), $UI_WINDOW_TITLE, $PRODUCT_TITLE & " requires the following Interaction Center license(s):" & @CRLF & @CRLF & _
	   "Interaction Process Automation" & @CRLF & _
	   "Proactive Patient Care Template" & @CRLF & @CRLF & _
	   "Contact your partner, or visit: http://license.inin.com, to obtain an updated software license file. Apply the file from within Interaction Administrator." )

		If ( ( ( $msgBoxOK == $MB_OK ) And ( $retVal == $IDOK ) ) Or ( ( $msgBoxOK == $MB_OKCANCEL ) And ( $retVal == $IDCANCEL ) ) ) Then Exit

	EndIf

	; IC Version
	#comments-start
	If ( ( ( $IC_VERSION_MJR == 15 ) And ( $IC_VERSION_MNR < 3 ) ) Or ( $IC_VERSION_MJR < 15 ) ) Then

		Local $retVal = MsgBox( ( $msgBoxOK + 48 + 4096 ), $UI_WINDOW_TITLE, _
		"The offical release version of " & $PRODUCT_TITLE & " is validated on server software for  Interaction Center 2015,  " & _
		"or later." & Chr(13) & Chr(13) & "Refer to the accompanying documentation for more information and technical support resources." )

		If ( ( ( $msgBoxOK == $MB_OK ) And ( $retVal == $IDOK ) ) Or ( ( $msgBoxOK == $MB_OKCANCEL ) And ( $retVal == $IDCANCEL ) ) ) Then Exit

	EndIf
	#comments-end

	While ( Not ( ProcessExists( "NotifierU.EXE" ) And ProcessExists( "ProcessAutomationServerU.EXE" ) ) )

		Local $retVal = MsgBox( 5 + 48 + 4096, $UI_WINDOW_TITLE, "Interaction Center and Process Automation Server must be running to continue with installation." )

		If ( $retVal == 2 ) Then

			Exit

		EndIf

	WEnd

	While ( ProcessExists( "IDU.EXE" ) )

		Local $retVal = MsgBox( 5 + 48 + 4096, $UI_WINDOW_TITLE, "Close Interaction Designer (IDU.EXE, for all users) before continuing with installation." )

		If ( $retVal == 2 ) Then

			Exit

		EndIf

	WEnd


	MsgBox( 64 + 4096, $UI_WINDOW_TITLE, "This installer can simplify some aspects of " & $PRODUCT_TITLE & " setup and configuration. " & _
	"After installation, refer to the solution's documentation to verify automated setup activities and for further customization options." )

;

;	==========

	Func _InstallEverything()

		Local Const $FILE_PATH_CONFIG_DS = $FILE_PATH_INSTALL_TEMP & "\IC\DS" & "\"
		Local Const $FILE_PATH_CONFIG_PAA = $FILE_PATH_INSTALL_TEMP & "\IC\PAA" & "\"

		Local Const $FILE_PATH_CONFIG_IHD = $IC_I3_IC_PATH & "Handlers\Custom\" & $APP_SUBFOLDER & "\"

		Local Const $FILE_PATH_CONFIG_TXT = $IC_LOCAL_RESOURCES & "\" & "TXT" & "\"
		Local Const $FILE_PATH_CONFIG_WAV = $IC_LOCAL_RESOURCES & "\" & "WAV" & "\"

		Local Const	$FILE_PATH_CONFIG_REF = $IC_LOCAL_RESOURCES & "\" & "Reference" & "\"
		Local Const	$FILE_PATH_CONFIG_WEB = $FILE_PATH_CONFIG_REF & "\" & "Web Setup" & "\"
		Local Const	$FILE_PATH_CONFIG_DBA = $FILE_PATH_CONFIG_REF & "\" & "Database Setup" & "\"

		Local Const	$FILE_PATH_CONFIG_WI = $IC_SHARE_RESOURCES & "\" & "WI"
		Local Const $FILE_PATH_CONFIG_WI_STATUS = $FILE_PATH_CONFIG_WI & "\" & "STATUS" & "\"
		Local Const $FILE_PATH_CONFIG_WI_PAGE =  $FILE_PATH_CONFIG_WI & "\" & "PAGE" & "\"

		_InstallProgress( 0, "Creating Directories" )  ;  ==========

		DirCreate( $FILE_PATH_CONFIG_DS )
		DirCreate( $FILE_PATH_CONFIG_PAA )
		DirCreate( $FILE_PATH_CONFIG_IHD )

		DirCreate( $IC_LOCAL_RESOURCES )

		DirCreate( $FILE_PATH_CONFIG_TXT )
		DirCreate( $FILE_PATH_CONFIG_WAV )

		DirCreate( $IC_SHARE_RESOURCES )
		DirCreate( $FILE_PATH_CONFIG_WI )
		DirCreate( $FILE_PATH_CONFIG_REF )
		DirCreate( $FILE_PATH_CONFIG_WEB )
		DirCreate( $FILE_PATH_CONFIG_DBA )
		DirCreate( $FILE_PATH_CONFIG_WI_STATUS )
		DirCreate( $FILE_PATH_CONFIG_WI_PAGE )


		_InstallProgress( 0, "Copying Configuration" )  ;  ==========

		FileInstall( "..\..\Structured Parameters.XML", $FILE_PATH_CONFIG_DS, 1 )

		_InstallProgress()

		FileInstall( "..\..\Data Sources.XML", $FILE_PATH_CONFIG_DS, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers.XML", $FILE_PATH_CONFIG_DS, 1 )

		_InstallProgress()

		_InstallProgress( 0, "Applying Configuration" )  ;  ==========

		_InstallDirectoryServices( $FILE_PATH_CONFIG_DS & "Structured Parameters.XML" )

		_InstallProgress()

		_InstallDirectoryServices( $FILE_PATH_CONFIG_DS & "Data Sources.XML" )

		_InstallProgress()

		_InstallDirectoryServices( $FILE_PATH_CONFIG_DS & "Handlers.XML" )

		_InstallProgress()


		_InstallProgress( 0, "Copying Handlers" )  ;  ==========

		FileInstall( "..\..\Handlers\ApptConfCallStation.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfDisconnect.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfFileDelete.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfFileExists.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfGetInput.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfPlaceCall.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfPlayPhrase.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfPlayTone.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfRecStart.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfRecStop.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfStructParam.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfTransferCall.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfLineAvailable.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileInstall( "..\..\Handlers\ApptConfLineAcquire.ihd", $FILE_PATH_CONFIG_IHD, 1 )

		_InstallProgress()

		FileCreateShortcut( $FILE_PATH_CONFIG_IHD, $FILE_PATH_CONFIG_REF & "Solution Handlers.LNK" )

		_InstallProgress()

		FileInstall( "..\..\Handlers\Handlers.LST", $FILE_PATH_CONFIG_IHD, 1 )


		_InstallProgress( 0, "Publishing Handlers" )  ;  ==========

		_InstallHandlers( "Handlers.LST", $FILE_PATH_CONFIG_IHD )


		_InstallProgress( 0, "Copying Images" )  ;  ==========

		FileInstall( "..\..\WI\PAGE\00.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\01.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\02.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\2B.JPG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\03.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\04.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\05.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\06.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\07.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\08.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\09.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\10.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\99.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\AA.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\BB.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\CC.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\PAGE\DD.PNG", $FILE_PATH_CONFIG_WI_PAGE, 1 )

		_InstallProgress()




		_InstallProgress( 0, "Copying Images" )  ;  ==========

		FileInstall( "..\..\WI\STATUS\---.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\00.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\01.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\02.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\03.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\04.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\05.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\06.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\07.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\08.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\09.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\10.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )

		_InstallProgress()

		FileInstall( "..\..\WI\STATUS\12.PNG", $FILE_PATH_CONFIG_WI_STATUS, 1 )



		_InstallProgress( 0, "Copying Processes" )  ;  ==========


		FileInstall( "..\..\IPA\Appointment - Callback.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Email.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Text-SMS.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Confirm.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - English.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Linguist.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Response.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Schedule.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()

		FileInstall( "..\..\IPA\Appointment - Studio.IPAExport", $FILE_PATH_CONFIG_PAA, 1 )

		_InstallProgress()



		_InstallProgress( 0, "Importing Processes" )  ;  ==========

		_InstallProcess( "Appointment - Callback.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Email.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Response.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Text-SMS.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - English.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Linguist.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Confirm.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Schedule.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		_InstallProcess( "Appointment - Studio.IPAExport", $FILE_PATH_CONFIG_PAA )

		_InstallProgress(12)

		FileCreateShortcut( $FILE_PATH_CONFIG_PAA, ( $FILE_PATH_CONFIG_REF & "Process Templates.LNK" ) )



		_InstallProgress( 0, "Copying Web Application" )  ;  ==========

		FileInstall( "Web Application.ZIP", $FILE_PATH_CONFIG_WEB, 1 )



		_InstallProgress( "Copying Web Service" )  ;  ==========

		FileInstall( "Web Service.ZIP", $FILE_PATH_CONFIG_WEB, 1 )

		_InstallProgress()



		_InstallProgress( 0, "Creating Shortcuts" )  ;  ==========

		FileCreateShortcut( "%WINDIR%\SysWOW64\ODBCAD32.EXE", ( $FILE_PATH_CONFIG_DBA & "Windows Data Sources.LNK" ) )

		_InstallProgress()

		FileInstall( "Source Package.ZIP", $FILE_PATH_CONFIG_WEB, 1 )

		_InstallProgress()

		FileCreateShortcut( "IASHELLU.EXE", ( $FILE_PATH_CONFIG_DBA & "IC Data Sources.LNK" ) )

		FileInstall( "Microsoft ASP.NET MVC 3.URL", $FILE_PATH_CONFIG_WEB, 1 )

		_InstallProgress()



		_InstallProgress( 0, "Copying Documentation" )  ;  ==========

		FileInstall( "..\..\DOC\Installation Guide.PDF", $FILE_PATH_CONFIG_REF, 1 )

		_InstallProgress()

		FileInstall( "..\..\DOC\Developer Guide.PDF", $FILE_PATH_CONFIG_REF, 1 )

		_InstallProgress()

		FileInstall( "..\..\DOC\User Guide.PDF", $FILE_PATH_CONFIG_REF, 1 )

		_InstallProgress()


		_InstallProgress()  ;  ======================================

		MsgBox( 64 + 4096, $UI_WINDOW_TITLE, "The automated setup activities for " & $PRODUCT_TITLE & " are complete." & @CRLF & @CRLF & _
		"Refer to the solution's documentation to verify the applied settings and for further customization options." & @CRLF & @CRLF & _
		"Operations: " & Int( $progressCount ) )

		ShellExecute( "EXPLORER.EXE", ( "/e /root," & Chr( 34 ) & $FILE_PATH_CONFIG_REF & Chr( 34 ) ) )

		Exit


	EndFunc

	Func _InstallProgress( $factor = 1, $subText = "" )

		$progressCount += $factor
		$progressPercent = ( $progressCount / $progressTotal ) * 100


		If ( $progressCount == 0 ) Then

			ProgressOn( $PRODUCT_TITLE, ( "Installing on " & $IC_HOSTNAME ), "", -1, -1, 16 )

		ElseIf ( $progressCount == $progressTotal ) Then

			ProgressOff()
			Return

		EndIf


		If ( $subText <> "" ) Then

			ProgressSet( $progressPercent, $subText & "..." )

		Else

			ProgressSet( $progressPercent )

		EndIf



	EndFunc


	Func _InstallHandlers( $fileListName, $fileWorkingFolder )

		Local $resource = $IC_HOSTNAME
		Local $roleName = "IC Administrator"

		Local $credRecall = nt_recall( $resource, $roleName )
		Local $userName = $credRecall[ $NT_AUTH_USER_IDX ]
		Local $userPass = $credRecall[ $NT_AUTH_PASSWORD_IDX ]

		Local $fileSingleName = StringReplace( $fileListName, ".LST", ".DAT" )
		Local $outFileName = $fileWorkingFolder & $fileSingleName

		Local $in = FileOpen( ( $fileWorkingFolder & $fileListName ), 0 )
		Local $status = 0


		While ( $status == 0 )

			Local $published = 0
			Local $response = 4
			Local $attempts = 0
			Local $displayMode = @SW_HIDE

			Local $out = FileOpen( ( $fileWorkingFolder & $fileSingleName ), 2 )
			Local $line = FileReadLine( $in )

			$status = @error

			FileWriteLine( $out, $line )
			FileClose( $out )

			While ( ( $response == 4 ) And ( $published == 0 ) )

				ShellExecute( "IDU.EXE", _
					"/notifier=" & $resource & " " & _
					"/user=" & $userName & " " & _
					"/password=" & $userPass & " " & _
					"/publish:" & $outFileName, _
					$fileWorkingFolder, "", $displayMode )

				$attempts += 1

				$published = ProcessWaitClose( "IDU.EXE", 30 )

				If ( $published <> 1 ) Then

					ProcessClose( "IDU.EXE" )

					$response =	MsgBox( 2 + 48 + 256 + 262144, $UI_WINDOW_TITLE, "The following handler did not import or publish as expected:" & _
					@CRLF & @CRLF & $line & @CRLF & @CRLF & "Attempts: " & $attempts )

					$displayMode = @SW_SHOWNORMAL

				EndIf

				If ( $response == 5 ) Then

					MsgBox( 48 + 262144, $UI_WINDOW_TITLE, "After installation completes, the following handler " & _
					"will require importing and publishing within Interaction Designer:" & @CRLF & @CRLF & $line )

				ElseIf ( $response == 3 ) Then

					Exit

				EndIf

			WEnd


			If( $published == 0 ) Then

				MsgBox( 48 + 4096, $UI_WINDOW_TITLE, "After installation completes, the following handler " & _
				"will require importing and publishing within Interaction Designer:" & @CRLF & @CRLF & $line )

			EndIf

			_InstallProgress(6)

		WEnd

		FileClose( $in )
		FileDelete( $fileSingleName )

		;EICPublisherU.EXE

	EndFunc

	Func _InstallProcess( $fileProcess, $fileWorkingFolder )

		Local Const $NT_AUTH_STATUS_IDX = 0
		Local Const $NT_AUTH_DOMAIN_IDX = 1
		Local Const $NT_AUTH_USER_IDX = 2
		Local Const $NT_AUTH_PASSWORD_IDX = 3
		Local Const $NT_AUTH_RECORDED_IDX = 4

		Local $resource = $IC_HOSTNAME
		Local $roleName = "IC Administrator"
		Local $credRecall = nt_recall( $resource, $roleName )
		Local $userName = $credRecall[ $NT_AUTH_USER_IDX ]
		Local $userPass = $credRecall[ $NT_AUTH_PASSWORD_IDX ]

		Local $published = 0
		Local $response = 4
		Local $attempts = 0
		Local $displayMode = @SW_HIDE

		While ( ( $response == 4 ) And ( $published == 0 ) )

			ShellExecute( "FlowUtil.EXE",  _
				"/user "  &  $userName  & " /password "  &  $userPass  & " " & _
				"/server "  &  $IC_HOSTNAME  &  " /import "  & _
				Chr(34) & $fileProcess & Chr(34) & " " & _
				"/publish",  $fileWorkingFolder,  "", $displayMode )

				$attempts += 1

			$published = ProcessWaitClose( "FlowUtil.EXE", 60 )

			If ( $published == 1 ) Then

				Return

			Else

				ProcessClose( "FlowUtil.EXE" )

				$response =	MsgBox( 2 + 48 + 256 + 262144, $UI_WINDOW_TITLE, "The following process did not import or publish as expected:" & _
				@CRLF & @CRLF & $fileProcess & @CRLF & @CRLF & "Attempts: " & $attempts )

				$displayMode = @SW_SHOWNORMAL

			EndIf

		WEnd

		If ( $response == 5 ) Then

			MsgBox( 48 + 262144, $UI_WINDOW_TITLE, "After installation completes, the following process " & _
			"will require importing and publishing within IPA Designer:" & @CRLF & @CRLF & $fileProcess )

		ElseIf ( $response == 3 ) Then

			Exit

		EndIf

	EndFunc


	Func _InstallDirectoryServices( $filePathIn )

		Local $filePathOut = StringReplace( $filePathIn, ".XML", ".DAT" )

		Local $in = FileOpen( $filePathIn, 0 )
		Local $out = FileOpen( $filePathOut, 2 )

		Local $clipTemp = ""

		Local $line = ""
		Local $status = 0

		While ( $status == 0 )

			$line = FileReadLine( $in )
			$status = @error

			$line = StringReplace( $line, "__:SERVER:__", $DS_SERVER_NAME )
			$line = StringReplace( $line, "__:SITE:__", $DS_SITE_NAME )

			$line = StringReplace( $line, "__:FilePathRoot_WAV:__", ( $IC_LOCAL_RESOURCES & "\" & "WAV" ) )
			$line = StringReplace( $line, "__:FilePathRoot_TXT:__", ( $IC_LOCAL_RESOURCES & "\" & "TXT" ) )
			$line = StringReplace( $line, "__:FilePathRoot_WI:__", ( $IC_SHARE_RESOURCES & "\" & "WI" ) )

			$line = StringReplace( $line, "__:ODBC-DSN:__", $dbOdbcDsn )
			$line = StringReplace( $line, "__:ODBC-DBO:__", $dbOdbcDbo )

			FileWriteLine( $out, $line )

		WEnd

		FileClose( $filePathIn )
		FileClose( $filePathOut )

		While WinExists( "DSEdit" )

			WinClose( "DSEdit" )

		WEnd

		BlockInput( 1 )

		Run( "DSEDITU.EXE", "",  @SW_HIDE)
		WinWait( "DSEdit" )

		_InstallProgress()

		WinClose( "DSEdit" )
		WinWaitClose( "DSEdit" )

		_InstallProgress()


		Run( "DSEDITU.EXE", "",  @SW_HIDE)
		WinWait( "DSEdit" )

		_InstallProgress()

		WinActivate( "DSEdit" )
		WinWaitActive( "DSEdit" )
		Send( "!f{ENTER}" )

		WinWait( "Open" )
		WinSetState( "Open", "", @SW_HIDE)
		WinActivate( "Open" )
		WinWaitActive( "Open" )

		$clipTemp = ClipGet()
		ClipPut( Chr(34) & $filePathOut & Chr(34) )
		Send( "^v{ENTER}" )
		ClipPut( $clipTemp )

		WinWaitActive( "DSEdit" )
		WinClose( "DSEdit" )

		BlockInput( 0 )

	EndFunc

;